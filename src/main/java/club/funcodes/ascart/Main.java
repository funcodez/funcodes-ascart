// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.ascart;

import static org.refcodes.cli.CliSugar.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.EnumOption;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.data.Delimiter;
import org.refcodes.exception.BugException;
import org.refcodes.graphical.BoxBorderMode;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.Terminal;
import org.refcodes.textual.AsciiArtBuilder;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.PixmapRatioMode;
import org.refcodes.textual.TextBorderBuilder;
import org.refcodes.textual.TextBoxStyle;
import org.refcodes.textual.VerboseTextBuilder;

/**
 * A minimum REFCODES.ORG enabled command line interface (CLI) application. Get
 * inspired by "https://bitbucket.org/funcodez".
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "ascart";
	private static final String TITLE = "ASC(II)ART";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES | See [https://www.metacodes.pro/manpages/ascart_manpage]";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );
	private static final String DESCRIPTION = "A command line tool for creating ASCII art (\"[ASC]II[ART]\") from plain text or image files (see [https://www.metacodes.pro/manpages/ascart_manpage]).";
	private static final String TEXT_PROPERTY = "text";
	private static final String OUTPUTFILE_PROPERTY = "outputFile";
	private static final String IMAGEFILE_PROPERTY = "imageFile";
	private static final String BORDER_STYLE_PROPERTY = "borderStyle";
	private static final String LINE_WIDTH_PROPERTY = "lineWidth";
	private static final String FONT_FAMILY_PROPERTY = "fontFamily";
	private static final String FONT_STYLE_PROPERTY = "fontStyle";
	private static final String COLOR_PALETTE_PROPERTY = "colorPalette";
	private static final String PADDING_PROPERTY = "padding";
	private static final String RATIO_MODE_PROPERTY = "ratioMode";
	private static final FontFamily DEFAULT_FONT_FAMILY = FontFamily.DIALOG;
	private static final FontStyle DEFAULT_FONT_STYLE = FontStyle.BOLD;
	private static final int DEFAULT_PADDING = 1;
	private static final AsciiColorPalette DEFAULT_COLOR_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY;
	private static final int DEFAULT_LINE_WIDTH = 80;
	private static final PixmapRatioMode DEFAULT_RATIO_MODE = PixmapRatioMode.CONSOLE;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String args[] ) {

		int theDefaultLineWidth = Terminal.getWidth();
		if ( theDefaultLineWidth < 0 ) {
			theDefaultLineWidth = DEFAULT_LINE_WIDTH;
		}

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final EnumOption<TextBoxStyle> theBorderStyleArg = enumOption( "border-style", TextBoxStyle.class, BORDER_STYLE_PROPERTY, "The style of the border surrounding the ASCII art: " + VerboseTextBuilder.asString( TextBoxStyle.values() ) );
		final EnumOption<FontFamily> theFontFamilyArg = enumOption( "font-family", FontFamily.class, FONT_FAMILY_PROPERTY, "The font family to use for the ASCII art text (defaults to <" + DEFAULT_FONT_FAMILY + ">): " + VerboseTextBuilder.asString( FontFamily.values() ) );
		final EnumOption<FontStyle> theFontStyleArg = enumOption( "font-style", FontStyle.class, FONT_STYLE_PROPERTY, "The font style to use for the ASCII art text (defaults to <" + DEFAULT_FONT_STYLE + ">): " + VerboseTextBuilder.asString( FontStyle.values() ) );
		final EnumOption<AsciiColorPalette> theColorPaletteArg = enumOption( "color-palette", AsciiColorPalette.class, COLOR_PALETTE_PROPERTY, "The color palette (chars) to use for the ASCII art (defaults to <" + DEFAULT_COLOR_PALETTE + ">): " + VerboseTextBuilder.asString( AsciiColorPalette.values() ) );
		final EnumOption<PixmapRatioMode> theRatioModeArg = enumOption( "ratio", PixmapRatioMode.class, RATIO_MODE_PROPERTY, "The ratio mode when rendering images (defaults to <" + DEFAULT_RATIO_MODE + ">): " + VerboseTextBuilder.asString( PixmapRatioMode.values() ) );
		final StringOption theImageFileArg = stringOption( 'i', "image-file", IMAGEFILE_PROPERTY, "The image file from which to create the ASCII art from." );
		final StringOption theOutputFileArg = stringOption( 'o', "output-file", OUTPUTFILE_PROPERTY, "The output file where to write the ASCII art to." );
		final StringOption theTextArg = stringOption( 't', "text", TEXT_PROPERTY, "The text message from which to create the ASCII art from." );
		final IntOption theLineWidthArg = intOption( "line-width", LINE_WIDTH_PROPERTY, "The line width to use when creating the ASCII art (defaults to current console's line width <" + theDefaultLineWidth + ">)." );
		final IntOption thePaddingArg = intOption( "padding", PADDING_PROPERTY, "The padding width to use when creating the ASCII art (defaults to <" + DEFAULT_PADDING + ">)." );
		final Flag theDebugFlag = debugFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();

		// @formatter:off
		final Term theArgsSyntax = cases(
			and( 
				theTextArg, any( theFontFamilyArg, theFontStyleArg , theColorPaletteArg, thePaddingArg, theBorderStyleArg, theOutputFileArg, theLineWidthArg, theVerboseFlag, theDebugFlag) 
			),
			and( 
				theImageFileArg, any( theRatioModeArg, theColorPaletteArg, thePaddingArg, theBorderStyleArg, theOutputFileArg, theLineWidthArg, theVerboseFlag, theDebugFlag) 
			),
			xor( theHelpFlag, and( theSysInfoFlag, any ( theVerboseFlag ) ) )
		);
		final Example[] theExamples = examples(
			example( "Render text with current console's width", theTextArg ), 
			example( "Render text with given width", theTextArg, theLineWidthArg ),
			example( "Render text with current console's width and given border", theTextArg, theBorderStyleArg ),
			example( "Render text with given width and border", theTextArg, theLineWidthArg, theBorderStyleArg ),
			example( "Render text with given width, padding and border", theTextArg, theLineWidthArg, thePaddingArg, theBorderStyleArg ),
			example( "Render text with given font family and font style", theTextArg, theFontFamilyArg, theFontStyleArg ),
			example( "Render text with given color palette", theTextArg, theColorPaletteArg ),
			example( "Render image with current console's width", theImageFileArg ),
			example( "Render image with given width", theImageFileArg, theLineWidthArg ),
			example( "Render image with current console's width and given border", theImageFileArg, theBorderStyleArg ),
			example( "Render image with given width, padding and border", theImageFileArg, theLineWidthArg, thePaddingArg, theBorderStyleArg ),
			example( "Render image with given width and border", theImageFileArg, theLineWidthArg, theBorderStyleArg ),
			example( "Render image with given color palette", theImageFileArg, theColorPaletteArg ),
			example( "Render image with given ration", theImageFileArg, theRatioModeArg ),
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).withEscapeCodesEnabled( false ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isVerbose = theCliHelper.isVerbose();

		try {
			final String theText = theArgsProperties.get( theTextArg );
			final String theImageFileName = theArgsProperties.get( theImageFileArg );
			final String theOutputFileName = theArgsProperties.get( theOutputFileArg );
			final TextBoxStyle theBorderStyle = theArgsProperties.getEnum( TextBoxStyle.class, theBorderStyleArg );
			final int thePadding = theArgsProperties.getIntOr( thePaddingArg, ( theBorderStyle != null ) ? DEFAULT_PADDING : 0 );
			final int theLineWidth = theArgsProperties.getIntOr( theLineWidthArg, theDefaultLineWidth );
			final AsciiColorPalette theColorPalette = theArgsProperties.getEnumOr( theColorPaletteArg, DEFAULT_COLOR_PALETTE );
			final FontFamily theFontFamily = theArgsProperties.getEnumOr( theFontFamilyArg, DEFAULT_FONT_FAMILY );
			final FontStyle theFontStyle = theArgsProperties.getEnumOr( theFontStyleArg, DEFAULT_FONT_STYLE );
			final PixmapRatioMode theRatioMode = theArgsProperties.getEnumOr( theRatioModeArg, DEFAULT_RATIO_MODE );

			if ( isVerbose ) {
				if ( theText != null && theText.length() != 0 ) {
					LOGGER.info( "Text = " + theText );
				}
				if ( theBorderStyle != null ) {
					LOGGER.info( "Encoding = " + theBorderStyle );
				}
				if ( theLineWidth != -1 ) {
					LOGGER.info( "Line width = " + theLineWidth );
				}
				LOGGER.info( "Color palette = " + theColorPalette );
			}

			OutputStream theOutputStream = Execution.toBootstrapStandardOut();
			if ( theOutputFileName != null && theOutputFileName.length() != 0 ) {
				File theOutputFile = new File( theOutputFileName );
				if ( isVerbose ) {
					LOGGER.info( "Output file = \"" + theOutputFileName + "\" (<" + theOutputFile.getAbsolutePath() + ">)" );
				}
				theOutputStream = new FileOutputStream( theOutputFile );
			}

			int theCanvasWidth = theLineWidth - thePadding * 2;
			if ( theBorderStyle != null ) {
				theCanvasWidth -= 2;
			}

			String[] theAsciiArt = null;

			if ( theText != null ) {
				if ( isVerbose ) {
					LOGGER.info( "Font family = " + theFontFamily );
					LOGGER.info( "Font style = " + theFontStyle );
				}
				theAsciiArt = new AsciiArtBuilder().withPixmapRatioMode( theRatioMode ).withAsciiColorPalette( theColorPalette ).withFontSize( 12 ).withColumnWidth( theCanvasWidth ).withFontStyle( theFontStyle ).withFontFamily( theFontFamily ).withText( theText ).toStrings();

			}
			else if ( theImageFileName != null && theImageFileName.length() != 0 ) {
				final File theImageFile = new File( theImageFileName );
				InputStream theImageStream;
				if ( isVerbose ) {
					LOGGER.info( "Image file = \"" + theImageFileName + "\" (<" + theImageFile.getAbsolutePath() + ">)" );
					LOGGER.info( "Ratio mode = " + theRatioMode );
				}
				if ( !theImageFile.exists() || !theImageFile.isFile() ) {
					theImageStream = Main.class.getResourceAsStream( theImageFileName.startsWith( Delimiter.PATH.getChar() + "" ) ? theImageFileName : Delimiter.PATH.getChar() + theImageFileName );
					if ( theImageStream == null ) {
						throw new FileNotFoundException( "No file \"" + theImageFileName + "\" (<" + theImageFile.getAbsolutePath() + ">) found!" );
					}
				}
				else {
					theImageStream = new FileInputStream( theImageFile );
				}
				try {
					theAsciiArt = new AsciiArtBuilder().withPixmapRatioMode( theRatioMode ).withAsciiColorPalette( theColorPalette ).withColumnWidth( theCanvasWidth ).withImageInputStream( theImageStream ).toStrings();
				}
				catch ( Exception e ) {
					throw new RuntimeException( "Cannot process image file <" + theImageFile.getAbsolutePath() + "> (propably wrong image format)!", e );
				}
			}
			else {
				throw new BugException( "We encountered a bug, none argument was processed!" );
			}

			if ( thePadding != 0 ) {
				theAsciiArt = new TextBorderBuilder().withBoxBorderMode( BoxBorderMode.ALL ).withText( theAsciiArt ).withBorderWidth( thePadding ).withBorderChar( ' ' ).toStrings();
			}

			if ( theBorderStyle != null ) {
				theAsciiArt = new TextBorderBuilder().withText( theAsciiArt ).withBoxBorderMode( BoxBorderMode.ALL ).withTextBoxStyle( theBorderStyle ).toStrings();
			}

			if ( isVerbose ) {
				LOGGER.printTail();
			}

			try ( PrintWriter theWriter = new PrintWriter( theOutputStream ) ) {
				for ( int i = 0; i < theAsciiArt.length; i++ ) {
					theWriter.println( theAsciiArt[i] );
				}
			}
		}
		catch ( Throwable e ) {
			theCliHelper.exitOnException( e );
		}
	}
}
